from account.models import Account


class RegisterInput:
    def __init__(self, email, password, name):
        self.email = email
        self.password = password
        self.name = name

class Register:
    def __init__(self, register_input):
        self.register_input = register_input
    print("register.py")

    def process(self):
        account = Account.objects.create(
            email=self.register_input.email,
            password=self.register_input.password,
            name=self.register_input.name
        )
        return account
