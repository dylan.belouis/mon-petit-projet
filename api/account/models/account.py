from django.db import models
# i need choice list for status
class Account(models.Model):

    STATUS_CHOICES = (
        ('simple_user', 'Simple User'),
        ('developper', 'Developper'),
    )
    name = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    email = models.EmailField()
    status = models.CharField(max_length=100, choices=STATUS_CHOICES, default='simple_user')


    def __str__(self):
        return self.name
