from django.db import models

class Project(models.Model):

    CATEGORIES_CHOICES = (
        ('ecommerce', 'Ecommerce'),
        ('environnement', 'Environnement'),
        ('education', 'Education'),
        ('sante', 'Sante'),
        ('other', 'Autre'),

    )
    PROJECT_TYPE_CHOICES = (
        ('library', 'Library'),
        ('application', 'Application'),
        ('website', 'Website'),
        ('package', 'Package'),
        ('other', 'Autre'),
    )
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    categorie = models.CharField(max_length=100, choices=CATEGORIES_CHOICES, default='simple_user')
    owner = models.ForeignKey('auth.User', related_name='projects', on_delete=models.CASCADE)
    project_type = models.CharField(max_length=100, choices=PROJECT_TYPE_CHOICES, default='simple_user')
    members = models.ManyToManyField('auth.User', related_name='members', blank=True)




    def __str__(self):
        return self.name
